﻿namespace Problem

module PrimeFactors =

    let generateFactors (num:uint32) = 
        let rec calc (number:uint32)  (d:uint32) result =
            match number >= d with
            | false -> result
            | true  ->  match number % d with
                        | 0u ->  calc (number / d) 2u (d :: result)
                        | _ -> calc number (d + 1u) result
        calc num 2u []





