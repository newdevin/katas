namespace Problem

module StringCalulator =
    open System
    
    let add (i:string) =
        let delim (input:string) =
            if input.StartsWith("//") then
                let fi = input.IndexOf("[")
                let li = input.IndexOf("]")
                let d = input.Substring(fi+1, li-fi-1)
                ([|d|] , input.Substring(li+2))
            else    
                ([|",";"\n"|], input)
        
        let nums (parts:string[]) = 
            match parts.Length with
            | 0 ->  [|0|]
            | _ ->  parts 
                    |>  Array.map ( fun n -> n |> int) 

        let sum (parts:string[]) =
                    let ints = parts |> nums
                    let minus = Array.filter( fun x -> x < 0) ints
                    if(minus.Length > 0) then 
                        failwithf "negatives not allowed - %s" (String.Join(",",minus))
                    ints |>  Array.sum
        
        let data = delim i
        
        match data with
        | _, "" -> 0
        | [|d|],x ->let parts = x.Split(d)
                    sum parts 
        | p,x ->    let sep = Array.map( fun c -> c |> char) p 
                    let parts = x.Split(sep )
                    sum parts

         
