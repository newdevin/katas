module Tests

open Xunit
open FsUnit

open Problem.StringCalulator
open Problem.PrimeFactors

[<Fact>]
let ``Empty strings return zero`` () =
    let expected = 0
    let actual = add ""
    actual |> should equal expected

[<Theory>]
[<InlineData("1",1)>]
[<InlineData("2",2)>]
[<InlineData("3",3)>]
[<InlineData("4",4)>]
[<InlineData("5",5)>]
[<InlineData("6",6)>]
[<InlineData("//[.]\n1.2", 3)>]
[<InlineData("//[**]\n1**2", 3)>]
[<InlineData("3,2,5", 10)>]
[<InlineData("3\n2,5", 10)>]

let ``A single input retuns the number itself``(input:string , result:int)=
    
    let actual = add input ;
    actual |> should equal result;

[<Fact>]
let ``Using a -ve number throws exception``() =
    let input = "2,-1";
    ( fun () -> add input |> ignore ) |> should throw  typeof<System.Exception>
    


[<Fact>]
let ``Prime factors`` () =
    
    let nums = [2u;3u;4u;12u;2279269u]
    let expected = [[2u];[3u];[2u;2u];[3u;2u;2u];[137u ; 131u ; 127u]]
    let actual = nums |> List.map ( fun n -> generateFactors n);
    actual |> should equal expected

    
                       
    



